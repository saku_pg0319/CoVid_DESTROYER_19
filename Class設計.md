# C++プログラミング実習2 最終課題

## Class List
### Sceneオブジェクト関係
- GameObject... 動くやつ
- UI... インターフェース系。使いやすいように改良
- Text... テキスト。使いやすいようにしたい(願望)

### 必要コンポーネント類
- Physics... 物理系
- Collision... 衝突判定系

### Scene制御
- SceneManager... シーン制御系 -> リセット、遷移、
- 

### 動作制御(ofApp.cppのメソッド呼び出し)
<br> ☆ UpdateとDrawは異なるもの<br>
- SetUp... 最初の1フレーム
- Update... 毎フレーム呼び出し
- Draw... Update後に描画
- KeyPressed... int型キーコードが押された瞬間
- KeyHeld... int型キーコードが押されている間(新規)
- KeyReleased... int型キーコードが離された瞬間
- MouseMoved... 単純なカーソル移動の検出(ボタン未入力
- MouseDragged... ボタンを押しながらのカーソル移動の検出
- MousePressed... マウスボタン押下の瞬間
- MouseHeld... マウスボタン押下中(新規)
- MouseReleased... マウスボタンを離した瞬間
- MouseEntered... カーソルがウィンドウ内に侵入した際に呼び出す処理
- MouseExited... カーソルがウィンドウ内から脱出した際に呼び出す処理
- 上記以外は多分使わない...

### 物の動き
- Transform... 位置、回転、大きさをまとめた構造体
#### 変数
- position... 位置(ofVec2)
- rotation... 回転(float)
- size... 大きさ(ofVec2)
#### メソッド
- Translate(float x, float y)... vector2(x, y)方向に移動。座標単位で移動。移動に成功した場合trueを返す。
- Rotate(float angle)... 基準の角度を変更することで回転
- SetPosition(float x, float y)... vector2(x, y)に移動。
- SetRotation(float angle)... angle(°)回転。2Dなので1次元で大丈夫

## オブジェクトのモデル

Example: ObjectA
<Component Class>
- Behavior
- Material
- Sprite
</Component Class>

## テキストオブジェクト
- テキスト
- Transform(位置、回転、大きさ)
- ofApp機能

## オブジェクト同士の参照の仕方に関して



