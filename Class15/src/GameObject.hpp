//
//  GameObject.hpp
//  Final
//
//  Created by hw20a029 on 2022/12/02.
//

#ifndef GameObject_hpp
#define GameObject_hpp

#include <iostream>
#include "ofApp.h"
#include "Transform.hpp"
#include "Sprite.hpp"

using namespace std;
using namespace glm;

//Sceneに登場させるオブジェクト(動く物
//継承して使う
class GameObject{
protected:
    string name;
    Sprite *sprite;
public:
    Transform transform;
    GameObject();
    virtual string GetName();
    virtual void SetUp();
    virtual void Draw();
};

#endif /* GameObject_hpp */
