//
//  GameSprashText.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#include "GameSprashText.hpp"

SprashText::SprashText() : Text(){
    transform.position = vec2(20, 600);
    transform.rotation = 0;
    transform.size = vec2(1, 1);
    SetFont("k8x12S.ttf", 30);
    text = "";
    color = vec4(255, 255, 255, 255);
    name = "GameSprash";
}

void SprashText::SetUp(){
    Text::SetUp();
}

void SprashText::Update(){
    Text::Update();
}

void SprashText::Draw(){
    Text::Draw();
}

