//
//  GameConditionText.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/03.
//

#ifndef GameConditionText_hpp
#define GameConditionText_hpp

#include <stdio.h>
#include "Text.hpp"

class GameCondition : public Text{
public:
    GameCondition();
    void SetUp() override;
    void Update() override;
    void Draw() override;
    void SetCondition(string str);
    void SetColor(int r, int g, int b, int a);
private:
};

#endif /* GameConditionText_hpp */
