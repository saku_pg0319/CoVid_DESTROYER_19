//
//  Score.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/29.
//

#ifndef Next_hpp
#define Next_hpp

#include <stdio.h>
#include "Text.hpp"

class NextText : public Text{
public:
    NextText();
    void SetUp() override;
    void Update() override;
    void Draw() override;
    
private:
    
};

#endif /* Next_hpp */
