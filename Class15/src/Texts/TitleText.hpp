//
//  TitleText.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#ifndef TitleText_hpp
#define TitleText_hpp

#include <stdio.h>
#include "Text.hpp"

class TitleText : public Text{
public:
    TitleText();
    void SetUp() override;
    void Update() override;
    void Draw() override;
    
private:
};

#endif /* TitleText_hpp */
