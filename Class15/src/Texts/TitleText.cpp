//
//  TitleText.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#include "TitleText.hpp"


TitleText::TitleText() : Text(){
    name = "TitleText";
}

void TitleText::SetUp(){
    Text::SetUp();
    text = "CoVid DESTROYER 19";
    SetFont("k8x12S.ttf", 80);
    color = vec4(255, 255, 255, 255);
    
    float X = GetTextWidth();
    float Y = GetTextHeight();
    cout<<"TextSize... "<<X/2<<", "<<Y/2<<endl;
    transform.position = vec2(ofGetWidth()/2-X/2, ofGetHeight()/2+Y/2-100);
    transform.rotation = 0;
    transform.size = vec2(1, 1);
    
}

void TitleText::Update(){
    Text::Update();
}

void TitleText::Draw(){
    Text::Draw();
}
