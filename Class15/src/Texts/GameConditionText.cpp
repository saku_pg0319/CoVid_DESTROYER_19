//
//  GameConditionText.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/03.
//

#include "GameConditionText.hpp"

GameCondition::GameCondition() : Text(){
    transform.position = vec2(20, 400);
    transform.rotation = 0;
    transform.size = vec2(1, 1);
    SetFont("k8x12S.ttf", 60);
    text = "";
    color = vec4(255, 255, 255, 255);
    name = "GameCondition";
}

void GameCondition::SetUp(){
    Text::SetUp();
}

void GameCondition::Update(){
    Text::Update();
}

void GameCondition::Draw(){
    Text::Draw();
}

void GameCondition::SetCondition(string str){
    text = str;
}

void GameCondition::SetColor(int r, int g, int b, int a){
    color = vec4(r, g, b, a);
}
