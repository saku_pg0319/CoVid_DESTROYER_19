//
//  CursorText.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#include "CursorText.hpp"

CursorText::CursorText(string _text, vec2 _pos) : Text(){
    name = _text;
    text = _text;
    pos = _pos;
}

void CursorText::SetUp(){
    Text::SetUp();
    SetFont("k8x12S.ttf", 30);
    color = vec4(255, 255, 255, 255);
    
    float X = GetTextWidth();
    float Y = GetTextHeight();
    //cout<<"TextSize... "<<X/2<<", "<<Y/2<<endl;
    transform.position = vec2(pos.x-X/2, pos.y+Y/2);
    transform.rotation = 0;
    transform.size = vec2(1, 1);
    
}

void CursorText::Update(){
    Text::Update();
}

void CursorText::Draw(){
    Text::Draw();
}
