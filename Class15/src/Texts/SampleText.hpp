//
//  SampleText.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/16.
//

#ifndef SampleText_hpp
#define SampleText_hpp

#include <stdio.h>
#include "Text.hpp"

class SampleText : public Text{
public:
    float t = 0;
    int score = 0;
    SampleText();
    void SetUp() override;
    void Update() override;
    void Draw() override;
    void KeyPressed(int keycode) override;
};

#endif /* SampleText_hpp */
