//
//  Score.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/29.
//

#ifndef Score_hpp
#define Score_hpp

#include <stdio.h>
#include "Text.hpp"

class Score : public Text{
public:
    int score;
    Score();
    void SetUp() override;
    void Update() override;
    void Draw() override;
    
    void AddScore(int scoreCount); //スコアの加減
    
    int GetScore(); //現時点のスコアの取得
private:
    int frame = 0;
};

#endif /* Score_hpp */
