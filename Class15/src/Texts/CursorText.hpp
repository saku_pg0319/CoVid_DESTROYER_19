//
//  CursorText.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#ifndef CursorText_hpp
#define CursorText_hpp

#include <stdio.h>
#include "Text.hpp"

class CursorText : public Text{
public:
    CursorText(string _text, vec2 _pos);
    void SetUp() override;
    void Update() override;
    void Draw() override;
private:
    vec2 pos;
};
#endif /* CursorText_hpp */
