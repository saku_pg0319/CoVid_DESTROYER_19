//
//  Score.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/29.
//

#include "Score.hpp"

Score::Score() : Text(){
    transform.position = vec2(50, 100);
    transform.rotation = 0;
    transform.size = vec2(1, 1);
    SetFont("k8x12S.ttf", 40);
    text = "Score: " + to_string(score);
    color = vec4(255, 255, 255, 255);
    score = 0;
    name = "Score";
}

void Score::SetUp(){
    Text::SetUp();
}

void Score::Update(){
    Text::Update();
    if(score > 99999999){
        score = 99999999;
    }
    //スコアが8桁に満たない場合は0で埋める
    string scoreText = to_string(score);
    string s = "";
    if(scoreText.length() < 8){
        while(scoreText.length() + s.length() < 8){
            s+="0";
        }
    }
    text = "Score: " + s + to_string(score);
}

void Score::Draw(){
    Text::Draw();
}

void Score::AddScore(int scoreCount){
    score+=scoreCount;
}

int Score::GetScore(){
    return score;
}
