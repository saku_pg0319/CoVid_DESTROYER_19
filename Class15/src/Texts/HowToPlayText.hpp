//
//  HowToPlayText.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/03.
//

#ifndef HowToPlayText_hpp
#define HowToPlayText_hpp

#include <stdio.h>
#include "Text.hpp"

class HowToPlay : public Text{
public:
    HowToPlay();
    void SetUp() override;
    void Update() override;
    void Draw() override;
    
private:
};
#endif /* HowToPlayText_hpp */
