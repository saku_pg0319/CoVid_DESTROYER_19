//
//  Score.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/29.
//

#include "Next.hpp"

NextText::NextText() : Text(){
    transform.position = vec2(720, 100);
    transform.rotation = 0;
    transform.size = vec2(1, 1);
    SetFont("k8x12S.ttf", 40);
    text = "Next";
    color = vec4(255, 255, 255, 255);
    name = "Next";
}

void NextText::SetUp(){
    Text::SetUp();
}

void NextText::Update(){
    Text::Update();
    
}

void NextText::Draw(){
    Text::Draw();
}


