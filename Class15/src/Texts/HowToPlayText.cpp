//
//  HowToPlayText.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/03.
//

#include "HowToPlayText.hpp"

HowToPlay::HowToPlay() : Text(){
    transform.position = vec2(20, 200);
    transform.rotation = 0;
    transform.size = vec2(1, 1);
    SetFont("k8x12S.ttf", 25);
    text = "[Move]\n  A:Left, D:Right, S:Drop\n[Rotate]\n  J:AntiClock, K:Clock";
    color = vec4(255, 255, 255, 255);
    name = "HowToPlay";
}

void HowToPlay::SetUp(){
    Text::SetUp();
}

void HowToPlay::Update(){
    Text::Update();
}

void HowToPlay::Draw(){
    Text::Draw();
}
