//
//  SampleText.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/16.
//

#include "SampleText.hpp"

SampleText::SampleText() : Text(){
    transform.position = vec2(100, 100);
    transform.rotation = 0;
    transform.size = vec2(1, 1);
    SetFont("k8x12S.ttf", 40);
    text = "Score: " + to_string(score);
    color = vec4(255, 255, 255, 255);
}

void SampleText::SetUp(){
    Text::SetUp();
}

void SampleText::Update(){
    Text::Update();
//    t+=0.1f;
//    transform.position.x = 400+200*sin(t/50*2*M_PI);
//    if(t >= 50){
//        t = 0;
//    }
    text = "Score: " + to_string(score);
}

void SampleText::Draw(){
    Text::Draw();
}

void SampleText::KeyPressed(int keycode){
    Text::KeyPressed(keycode);
    if(keycode == 'p'){
        score+=1;
    }
}
