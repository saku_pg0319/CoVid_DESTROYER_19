//
//  GameSprashText.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#ifndef GameSprashText_hpp
#define GameSprashText_hpp

#include <stdio.h>
#include "Text.hpp"

class SprashText : public Text{
public:
    SprashText();
    void SetUp() override;
    void Update() override;
    void Draw() override;
private:
};

#endif /* GameSprashText_hpp */
