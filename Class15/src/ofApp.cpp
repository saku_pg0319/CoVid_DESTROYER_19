#include "ofApp.h"
#include <vector>
#include "Scene.hpp"
/// <Scenes>
#include "Scenes/Samples/SampleScene0.hpp"
#include "Scenes/Samples/SampleScene1.hpp"
#include "Scenes/Main/PreStage.hpp"
#include "Scenes/Main/Title.hpp"
#include "Scenes/Main/Results.hpp"
#include "Scenes/Main/Ranking.hpp"
/// </Scenes>
///

string scoreFilePath = "scoreData.txt";

static int currentLoadSceneNum = 0;
Scene *currentLoadScene = nullptr;

PreStage *preStage01 = new PreStage();
Title *title = new Title();
Results *results = new Results();
Ranking *ranking = new Ranking();
vector<Scene *> scenes = {title, preStage01, results, ranking};

PlayerScore currentPlayerScore = PlayerScore("---", 0);

int Difficulty = 1;
vector<PlayerScore> playerScoreList;
string currentLoadFont;
float currentLoadFontSize;

///Load scene without destroying
void LoadScene(int nextSceneNum){
    currentLoadSceneNum = nextSceneNum;
    currentLoadScene = scenes[currentLoadSceneNum];
}

///Load scene and destroy previous scene
void LoadSceneWithDestroy(int nextSceneNum){
    cout<<"Destroy Loading"<<endl;
    int tmp = currentLoadSceneNum;
    currentLoadSceneNum = nextSceneNum;
    scenes[tmp] = nullptr;
    switch(tmp){
        case 0:
            scenes[tmp] = new Title();
            break;
        case 1:
            scenes[tmp] = new PreStage();
            break;
        case 2:
            scenes[tmp] = new Results();
            break;
        case 3:
            scenes[tmp] = new Ranking();
            break;
        default:
            break;
            
    }
    currentLoadScene = scenes[currentLoadSceneNum];
}

///Reload current scene with destroying
void ReloadScene(){
    LoadSceneWithDestroy(currentLoadSceneNum);
}

void SetCurrentScore(string name, int score){
    currentPlayerScore.name = name;
    currentPlayerScore.score = score;
    cout<<"PlayerName: "<<name<<", Score: "<<score<<endl;
}

vector<PlayerScore> GetScoreList(){
    return playerScoreList;
}

PlayerScore GetCurrentScore(){
    return currentPlayerScore;
}

void SetScoreList(vector<PlayerScore> vps){
    playerScoreList = vps;
    ofstream ofs(scoreFilePath);
    for(PlayerScore ps : vps){
        cout<<ps.name<<" "<<ps.score<<endl;
        ofs<<ps.name<<" "<<to_string(ps.score)<<endl;
    }
    cout<<"Wrote score"<<endl;
    ofs.close();
}

/// Get Adress of CurrentLoadScene
intptr_t GetCurrentLoadScene(){
    /*
     取得したアドレスは別オブジェクトなどから呼び出し、
     */
    intptr_t scenePtr = (intptr_t)currentLoadScene;
    return scenePtr;
}

intptr_t GetOtherActor(string objName){
    Scene *currentScene = (Scene *)GetCurrentLoadScene();
    for(auto obj : currentScene->GetActorList()){
        string s = obj->GetName();
        if(objName == s){
            return (intptr_t)obj;
        }
    }
    return (intptr_t)nullptr;
}

intptr_t GetOtherGameObj(string objName){
    Scene *currentScene = (Scene *)GetCurrentLoadScene();
    for(auto obj : currentScene->GetGameObjList()){
        string s = obj->GetName();
        if(objName == s){
            return (intptr_t)obj;
        }
    }
    return (intptr_t)nullptr;
}

intptr_t GetOtherText(string objName){
    Scene *currentScene = (Scene *)GetCurrentLoadScene();
    for(auto obj : currentScene->GetTextList()){
        string s = obj->GetName();
        if(objName == s){
            return (intptr_t)obj;
        }
    }
    return (intptr_t)nullptr;
}

void SetDifficulty(int _difficulty){
    Difficulty = _difficulty;
}
int GetDifficulty(){
    return Difficulty;
}

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetDataPathRoot("../Resources/");
    ofSetVerticalSync(true);
    ofSetFrameRate(60);
    
    ifstream ifs(scoreFilePath);
    string str;
    if (ifs.fail()) {
            std::cerr << "Failed to open file." << std::endl;
            return -1;
        }
    while(getline(ifs, str)){
        string name;
        int score;
        for(int i = 0; i < str.length(); i++){
            if(str[i] == ' '){
                //空白が見つかったらそこまでを名前として記録
                name = str.substr(0,i);
                string s = str.substr(i+1,str.length()-i-1);
                score = stoi(s);
                cout<<name<<" "<<score<<endl;
                playerScoreList.push_back(PlayerScore(name, score));
                break;
            }
        }
    }
    ifs.close();
    if(playerScoreList.size() < 10){
        cout<<"Auto Score Fill "<<playerScoreList.size()<<endl;
        for(int i = 0; i < 10 - playerScoreList.size(); i++){
                playerScoreList.push_back(PlayerScore("---", 0));
            }
    }
//    for(int i = 0; i < 10; i++){
//        playerScoreList.push_back(PlayerScore("---", 0));
//    }
    currentLoadScene = scenes[currentLoadSceneNum];
    currentLoadScene->SetUp();
}

//--------------------------------------------------------------
void ofApp::update(){
    bool bw = ofGetWidth() != windowSizeWidth;
    bool bh = ofGetHeight() != windowSizeHeight;
    if(bw || bh){
        ofSetWindowShape(windowSizeWidth, windowSizeHeight);
    }
    currentLoadScene->Update();
}

//--------------------------------------------------------------

void ofApp::draw(){
    currentLoadScene->Draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    currentLoadScene->KeyPressed(key);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
