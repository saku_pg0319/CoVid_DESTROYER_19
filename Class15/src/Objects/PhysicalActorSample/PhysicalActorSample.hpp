//
//  PhysicalActorSample.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#ifndef PhysicalActorSample_hpp
#define PhysicalActorSample_hpp

#include <stdio.h>
#include "PhysicalActor.hpp"
#include "Sprite.hpp"

class PhysicalActorSample : public PhysicalActor{
private:
    Sprite *sprite;
public:
    PhysicalActorSample();
    void SetUp();
    void Update();
    void Draw();
    void KeyPressed(int keycode);
    void KeyReleased(int keycode);
    void MouseMoved(int x, int y);
    void MousePressed(int x, int y, int button);
    void MouseReleased(int x, int y, int button);
    void MouseEntered(int x, int y);
    void MouseExited(int x, int y);
};

#endif /* PhysicalActorSample_hpp */
