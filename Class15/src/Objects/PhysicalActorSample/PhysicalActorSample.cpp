//
//  PhysicalActorSample.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#include "PhysicalActorSample.hpp"

PhysicalActorSample::PhysicalActorSample() : PhysicalActor(){
    physics.velocity = vec2(2, -2);
    physics.gravityScale = 0.01f;
    physics.bounciness = 1.0f;
    physics.staticFriction = 0;
    physics.dynamicFriction = 0;
    
    transform.position.x = 10;
    transform.position.y = 700;
    transform.rotation = 0;
    transform.size = vec2(10, 10);
    
    vec4 colorVec = vec4(100, 255, 100, 255);
    sprite = new Sprite(false, circle, colorVec);
    sprite->useFillColor = true;
    
    name = "PASample";
    //cout<<"Gene "<<name<<endl;
}

void PhysicalActorSample::SetUp(){
    //cout<<"SetUp"<<name<<endl;
}

void PhysicalActorSample::Update(){
    PhysicalActor::Update();
}

void PhysicalActorSample::Draw(){
    sprite->DrawVisual(transform.position.x, transform.position.y, transform.size.x, transform.size.y);
}

void PhysicalActorSample::KeyPressed(int keycode){
    if(keycode == 'h'){
        LoadSceneWithDestroy(0);
        //cout<<"Pressed H"<<endl;
    }
}
void PhysicalActorSample::KeyReleased(int keycode){
    
}
void PhysicalActorSample::MouseMoved(int x, int y){
    
}
void PhysicalActorSample::MousePressed(int x, int y, int button){
    
}
void PhysicalActorSample::MouseReleased(int x, int y, int button){
    
}
void PhysicalActorSample::MouseEntered(int x, int y){
    
}
void PhysicalActorSample::MouseExited(int x, int y){
    
}
