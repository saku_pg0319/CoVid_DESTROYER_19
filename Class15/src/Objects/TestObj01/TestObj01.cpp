//
//  TestObj01.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#include "TestObj01.hpp"

TestObj01::TestObj01() : Actor(){
    transform.position.x = 1024/2;
    transform.position.y = 768/2;
    transform.rotation = 0;
    transform.size = vec2(10, 10);
    
    vec4 colorVec = vec4(255, 0, 0, 255);
    sprite = new Sprite(false, circle, colorVec);
    sprite->useFillColor = true;
    name = "TestObj";
    //cout<<"Gene "<<name<<endl;
}

void TestObj01::SetUp(){
    //cout<<"SetUp"<<name<<endl;
    middlePos = transform.position;
}

void TestObj01::Update(){
    transform.position = middlePos + vec2(50*cos(2*M_PI*i/50), 50*sin(2*M_PI*i/50));
    i++;
}

void TestObj01::Draw(){
    //cout<<"Draw"<<endl;
    sprite->DrawVisual(transform.position.x, transform.position.y, transform.size.x, transform.size.y);
}

void TestObj01::KeyPressed(int keycode){
    if(keycode == 'h'){
        LoadSceneWithDestroy(1);
        //cout<<"Pressed H"<<endl;
    }
}
