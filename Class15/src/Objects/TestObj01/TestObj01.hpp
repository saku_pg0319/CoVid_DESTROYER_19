//
//  TestObj01.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#ifndef TestObj01_hpp
#define TestObj01_hpp

#include <stdio.h>
#include "Actor.hpp"
#include "SampleScene0.hpp"

class TestObj01 : public Actor{
public:
    TestObj01();
    void SetUp();
    void Update();
    void Draw();
    void KeyPressed(int keycode);
private:
    Sprite *sprite;
    int i = 0;
    vec2 middlePos;
    
};

#endif /* TestObj01_hpp */
