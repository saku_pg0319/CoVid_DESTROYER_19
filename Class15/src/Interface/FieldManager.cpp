//
//  FieldManager.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/29.
//

#include "FieldManager.hpp"

FieldManager::FieldManager() : Actor(){
    name = "FieldManager";
}

void FieldManager::SetUp(){
    Actor::SetUp();
    Field *field = (Field *)GetOtherGameObj("Field");
    Score *score = (Score *)GetOtherText("Score");
    targetNexts = (NextBlocks *)GetOtherGameObj("NextBlocks");
    targetGC = (GameCondition *)GetOtherText("GameCondition");
    targetSpT0 = (SprashText *)GetOtherText("GameSprash");
    //cout<<"FieldManager: "<<field->GetName()<<endl;
    for(int i = 0; i < defaultFieldHeight; i++){
        for(int j = 0; j < defaultFieldWidth; j++){
            field->SetTileId(j, i, None);
        }
    }
    
    targetField = field;
    targetScoreText = score;
    //GenerateEasy();
    GenerateField();

    dbl = new DroppingBlockList();
    vec2 colors = dbl->GetNextBlocks(false);
    targetNexts->SetNextBlockColor(colors.x, colors.y);
    targetSpT0->transform.position = vec2(20, 440);
    
    WriteFieldCondition();
    startLevel = 0;
}

void FieldManager::Update(){
    bool f = targetField->GetIsFalling();
    bool d = targetField->GetIsDropping();
    bool s = targetField->GetIsSpawnedDropping();
    if(startLevel == 0){
        if(startFrame == 0){
            fadeLevel = 0;
        }
        startFrame++;
        fadeLevel+=255/maxStartFrame;
        targetGC->SetColor(255, 255, 255, fadeLevel);
        targetGC->text = "READY...";
        if(startFrame >= maxStartFrame){
            startLevel = 1;
            startFrame = 0;
        }
        return;
    }
    else if(startLevel == 1){
        startFrame++;
        fadeLevel-= (float)(255/maxStartFrame);
        if(startFrame >= maxStartFrame){
            targetGC->color = vec4(255, 255, 255, 255);
            targetGC->text = "GO!";
            fadeLevel = 255;
            startLevel = 2;
            startFrame = 0;
        }
        return;
    }
    targetGC->color = vec4(255, 255, 255, fadeLevel);
    fadeLevel-=(float)(255/maxStartFrame);
    if(fadeLevel <= 0){
        targetGC->text = "";
    }
    
    //cout<<b<<endl;
    if(isCleared){
        targetGC->color = vec4(255, 255, 255, 255);
        targetGC->text = "CLEAR!";
        targetSpT0->color = vec4(255,255,255,255);
        targetSpT0->text = "R:Restart, Q:Quit\nSpace:Move to Result";
        return;
    }
    if(isGameOvered){
        targetField->backgroundColor = vec4(250, 50, 50, 50);
        targetGC->color = vec4(255, 0, 0, 255);
        targetGC->text = "GAME OVER!";
        targetSpT0->color = vec4(255,255,255,255);
        targetSpT0->text = "R:Restart, Q:Quit";
        return;
    }
    
    if(!f && !d && !s){
        //落ちもの発生
        chainCount = 0;
        if(targetField->GetVirusCount() == 0){
            isCleared = true;
            return;
        }
        
        if(targetField->GetTileId(2, 0) != None || targetField->GetTileId(3, 0) != None){
            cout<<"GameOver"<<endl;
            isGameOvered = true;
            return;
        }
        vec2 blocks = dbl->GetNextBlocks(true);
        vec2 colors = dbl->GetNextBlocks(false);
        targetNexts->SetNextBlockColor(colors.x, colors.y);
        targetField->SetTileId(2, 0, blocks.x);
        targetField->SetTileId(3, 0, blocks.y);
        //cout<<"Controller: Set Block"<<endl;
        targetField->ToggleIsDropping();
        bool boo = targetField->ToggleIsSpawnedDropping();
        //cout<<boo<<endl;
        //cout<<"Fall: "<<f<<", Drop: "<<d<<", Spawned: "<<s<<endl;
    }
    if(!f && d && s){
        //落ちものが落下中
        //何もしない
        //cout<<"Fall: "<<f<<", Drop: "<<d<<", Spawned: "<<s<<endl;
    }
    if(!f && !d && s){
        //落ちもの設置直後
        bool cct = CheckClearableTile();
        //cout<<"CCT: "<<cct<<endl;
        if(!cct){
            targetField->ToggleIsSpawnedDropping();
        }
        
        //cout<<"Fall: "<<f<<", Drop: "<<d<<", Spawned: "<<s<<endl;
    }
    if(f && !d && s){
        //落下処理
        for(int i = 0; i < defaultFieldWidth; i++){
            for(int j = 0; j < defaultFieldHeight; j++){
                if(targetField->GetTileId(i, j) >= RedFalling && targetField->GetTileId(i, j) <= GreenFalling){
                    targetField->SetTileId(i, j, None);
                }
            }
        }
        //cout<<"Fall: "<<f<<", Drop: "<<d<<", Spawned: "<<s<<endl;
    }
    
    
    
}

void FieldManager::KeyPressed(int keycode){
    if(keycode == 'c'){
        //CheckClearableTile();
    }
    if(keycode =='w'){
        cout<<"-------------------"<<endl;
        cout<<"- Current Condition\n"<<endl;
        WriteFieldCondition();
    }
    if(keycode == 'r'){
        ReloadScene();
    }
    if(isGameOvered || isCleared){
        
        if(keycode == 'q'){
            LoadSceneWithDestroy(0);
        }
    }
    
    if(isCleared){
        if(keycode == 32){
            SetCurrentScore("AAA", targetScoreText->score);
            LoadSceneWithDestroy(2);
            return;
        }
    }
}

bool FieldManager::CheckClearableTile(){
    /*
     - フィールド全体を参照する
     - 色付きブロックorウイルスを見つけたらつぎの処理
        - 縦方向にタイルを参照する
        - 同じ色のタイルがあったらカウント(cV)を1増やす
        - 上方向と下方向の両方を参照して、cV>=4ならその範囲のIdを"**CheckedTile"に変更する
        - ただし、参照は別の色が見つかった瞬間止める
        - 横方向にも同じ処理をする
     */
    ///<消える範囲を設定する>
    bool result = false; //落下処理、消去処理を実行したかどうか
    int blockCount = 0;
    int virusCount = 0;
    bool clearColors[3] = {false, false, false};
    for(int i = 0; i < defaultFieldWidth; i++){
        for(int j = 0; j < defaultFieldHeight; j++){
            int tileId = targetField->GetTileId(i, j);
            if(tileId != None && tileId != Void && tileId != Checker){
//                cout<<"FManager: Current Check... ("<<i<<", "<<j<<")"<<endl;
//                cout<<"FManager: Color is... "<<tileId<<endl;
                
                ///<縦方向参照>
                ///<上方向>
                int upperCount = CountSameColors(Up, vec2(i,j), 0, tileId);
                //cout<<"Looking Up: "<<upperCount<<endl;
                vec2 limitUpperTilePos;
                limitUpperTilePos = vec2(i, j-upperCount);
                ///</上方向>
                ///<下方向>
                int lowerCount = CountSameColors(Down, vec2(i,j), 0, tileId);
                vec2 limitLowerTilePos;
                limitLowerTilePos = vec2(i, j+lowerCount);
                //cout<<"Looking Down: "<<lowerCount<<endl;
                //cout<<"Vertical: "<<lowerCount+upperCount<<endl;
                //cout<<"FManager: Checked Count... "<<lowerCount+upperCount<<endl;
                if(lowerCount+upperCount >= 3){
                    //cout<<"FManager: Vertical Clear"<<endl;
                    result += true;
                    
                    for(int n = (int)limitUpperTilePos.y; n < limitLowerTilePos.y+1; n++){
                        if(targetField->GetTileId(i, n) >= RedVirus && targetField->GetTileId(i, n) <= GreenVirus){
                            virusCount++;
                            cout<<"FManager: Virus"<<endl;
                        }else if(targetField->GetTileId(i, n) >= RedBlock && targetField->GetTileId(i, n) <= GreenBlock){
                            blockCount++;
                        }
                        ///<色ごとのタイル変換>
                        switch(tileId){
                            case RedVirus:
                                targetField->SetTileId(i, n, RedCheckedTile);
                                clearColors[0] = true;
                                break;
                            case RedBlock:
                                targetField->SetTileId(i, n, RedCheckedTile);
                                clearColors[0] = true;
                                break;
                            case BlueVirus:
                                targetField->SetTileId(i, n, BlueCheckedTile);
                                clearColors[1] = true;
                                break;
                            case BlueBlock:
                                targetField->SetTileId(i, n, BlueCheckedTile);
                                clearColors[1] = true;
                                break;
                            case GreenVirus:
                                targetField->SetTileId(i, n, GreenCheckedTile);
                                clearColors[2] = true;
                                break;
                            case GreenBlock:
                                targetField->SetTileId(i, n, GreenCheckedTile);
                                clearColors[2] = true;
                                break;
                            default:
                                break;
                        }
                        ///</色ごとのタイル変換>
                        
                    }
                    //cout<<"FManager: Cleared!"<<endl;
                }
                else{
                    //cout<<"FManager: No Cleared!"<<endl;
                }
                ///</縦方向参照>
                ///<横方向>
                ///<左方向>
                int lefterCount = CountSameColors(Left, vec2(i,j), 0, tileId);
                //cout<<"Looking Left: "<<lefterCount<<endl;
                vec2 limitLefterTilePos;
                //lefterCount--; //終わったら-1する
                limitLefterTilePos = vec2(i-lefterCount, j);
                ///</左方向>
                ///<右方向>
                int righterCount = CountSameColors(Right, vec2(i,j), 0, tileId);
                //cout<<"Looking Right: "<<righterCount<<endl;
                vec2 limitRighterTilePos;
                limitRighterTilePos = vec2(i+righterCount, j);
                //cout<<"FManager: Checked Count... "<<lowerCount+upperCount<<endl;
                if(lefterCount+righterCount >= 3){
                    //cout<<"FManager: Horizontal Clear"<<endl;
                    
                    result += true;
                    for(int n = (int)limitLefterTilePos.x; n < limitRighterTilePos.x+1; n++){
                        if(targetField->GetTileId(n, j) >= RedVirus && targetField->GetTileId(n, j) <= GreenVirus){
                            virusCount++;
                            //cout<<"FManager: Virus"<<endl;
                        }else if(targetField->GetTileId(n, j) >= RedBlock && targetField->GetTileId(n, j) <= GreenBlock){
                            blockCount++;
                        }
                        ///<色ごとのタイル変換>
                        switch(tileId){
                            case RedVirus:
                                targetField->SetTileId(n, j, RedCheckedTile);
                                clearColors[0] = true;
                                virusCount++;
                                break;
                            case RedBlock:
                                targetField->SetTileId(n, j, RedCheckedTile);
                                clearColors[0] = true;
                                break;
                            case BlueVirus:
                                targetField->SetTileId(n, j, BlueCheckedTile);
                                clearColors[1] = true;
                                virusCount++;
                                break;
                            case BlueBlock:
                                targetField->SetTileId(n, j, BlueCheckedTile);
                                clearColors[1] = true;
                                break;
                            case GreenVirus:
                                targetField->SetTileId(n, j, GreenCheckedTile);
                                clearColors[2] = true;
                                virusCount++;
                                break;
                            case GreenBlock:
                                targetField->SetTileId(n, j, GreenCheckedTile);
                                clearColors[2] = true;
                                break;
                            default:
                                break;
                        }
                        ///</色ごとのタイル変換>
                        
                    }
                    //cout<<"FManager: Cleared!"<<endl;
                }
                else{
                    //cout<<"FManager: No Cleared!"<<endl;
                }
                ///</右方向参照>
                ///</横方向参照>
                
            }
        }///</for(int j = 0; j < defaultFieldHeight; j++)>
    }///</for(int i = 0; i < defaultFieldWidth; i++)>
    ///</消える範囲を設定する>
    ///<消える範囲をNoneに変える、残ったウイルス以外のブロックを落下対象に変える>
    bool isClearedVirus = false;
    for(int i = 0; i < defaultFieldWidth; i++){
        for(int j = defaultFieldHeight-1; j >= 0; j--){
            int tileId = targetField->GetTileId(i, j);
            if(tileId >= RedCheckedTile && tileId<= GreenCheckedTile){
                targetField->SetTileId(i, j, None);
                //cout<<"Clear!"<<endl;
                isClearedVirus = true;
            }
            //落下対象のブロックを設定する
            if(j+1 != defaultFieldHeight && (targetField->GetTileId(i, j+1) == None|| (targetField->GetTileId(i, j+1) >= RedFalling && targetField->GetTileId(i, j+1) <= GreenFalling))){
                switch(tileId){
                    case RedBlock:
                        targetField->SetTileId(i, j, RedFalling);
                        targetField->AddFallingBlockCondition(i, j, RedFalling);
                        result += true;
                        break;
                    case BlueBlock:
                        targetField->SetTileId(i, j, BlueFalling);
                        targetField->AddFallingBlockCondition(i, j, BlueFalling);
                        result += true;
                        break;
                    case GreenBlock:
                        targetField->SetTileId(i, j, GreenFalling);
                        targetField->AddFallingBlockCondition(i, j, GreenFalling);
                        result += true;
                        break;
                    default:
                        break;
                }
            }
            
        }
    }
    if(isClearedVirus){
        chainCount++;
    }
    ///</消える範囲をNoneに変える>
    int colorCount = 0;
    for(bool booleanColor : clearColors){
        //cout<<"Color... "<<booleanColor<<endl;
        colorCount = booleanColor? colorCount+1 : colorCount;
    }
    cout<<"Color Count... "<<colorCount<<endl;
    cout<<"Virus Cleared... "<<virusCount<<endl;
    cout<<"Clear Count... "<<virusCount+blockCount<<endl;
    int additionalScore = virusCount*10*(pow(2, 2*(chainCount-1))+(virusCount+blockCount)+2*(colorCount-1));
//    cout<<"Score Add... "<<additionalScore<<endl;
//    cout<<"Chains... "<<chainCount<<endl;
    targetScoreText->score+=additionalScore;
    return result;
}

void FieldManager::WriteFieldCondition(){
    for(int i = 0; i < defaultFieldHeight; i++){
        for(int j = 0; j < defaultFieldWidth; j++){
            string tileId = to_string(targetField->GetTileId(j, i));
            if(tileId.length() < 2){
                tileId = "0"+tileId;
            }
            cout<<tileId<<" ";
        }///</for(int j = 0; j < defaultFieldHeight; j++)>
        cout<<endl;
    }///</for(int i = 0; i < defaultFieldWidth; i++)>
}

bool FieldManager::IsSameColor(int baseColor, int targetColor){
    //入力されたtileIdの色が同じか判定する
    //探索対象はウイルス、ブロック、CheckedTile
    int reds[3] = {RedVirus, RedBlock, RedCheckedTile};
    int blues[3] = {BlueVirus, BlueBlock, BlueCheckedTile};
    int greens[3] = {GreenVirus, GreenBlock, GreenCheckedTile};
    bool isRed = false;
    bool isBlue = false;
    bool isGreen = false;
    for(int i : reds){
        if(baseColor == i){
            isRed = true;
            break;
        }
    }
    for(int i : blues){
        if(baseColor == i){
            isBlue = true;
            break;
        }
    }
    for(int i : greens){
        if(baseColor == i){
            isGreen = true;
            break;
        }
    }
    
    if(isRed){
        for(int i : reds){
            if(targetColor == i){
                return true;
            }
        }
    }
    else if(isBlue){
        for(int i : blues){
            if(targetColor == i){
                return true;
            }
        }
    }
    else if(isGreen){
        for(int i : greens){
            if(targetColor == i){
                return true;
            }
        }
    }
    else{
        return false;
    }
    return false;
    
}

void FieldManager::GenerateEasy(){
    targetField->SetTileId(0, 10, RedVirus);
    targetField->SetTileId(1, 10, RedVirus);
    targetField->SetTileId(3, 10, BlueVirus);
    targetField->SetTileId(4, 10, GreenVirus);
    targetField->SetTileId(5, 10, RedVirus);
    
    targetField->SetTileId(0, 9, RedVirus);
    targetField->SetTileId(2, 9, GreenVirus);
    targetField->SetTileId(5, 9, RedVirus);
    
    targetField->SetTileId(0, 8, RedVirus);
    targetField->SetTileId(1, 8, GreenVirus);
    targetField->SetTileId(4, 8, BlueVirus);
    targetField->SetTileId(5, 8, GreenVirus);
    
    targetField->SetTileId(4, 7, RedVirus);
    
    targetField->SetTileId(2, 6, GreenVirus);
    targetField->SetTileId(3, 6, RedVirus);
    targetField->SetTileId(5, 6, BlueVirus);
}

void FieldManager::GenerateField(){
    int widthCount = 0;
    int heightCount = defaultFieldHeight-1;
    srand(time(nullptr)%255);
    cout<<"Generating..."<<endl;
    targetField->SetDroppingFrame(GetDifficulty()*7);
    //cout<<"Difficulty is "<<GetDifficulty()<<endl;
    while(heightCount >= 10-GetDifficulty()+1){
        //cout<<"Generate line"<<heightCount<<endl;
        widthCount = 0;
        int noneCount = 0; //1段に空白は3個まで
        vector<int> settableColors = {RedVirus, BlueVirus, GreenVirus, None};
        while(widthCount < defaultFieldWidth){
            int sameColorCountSide = 0;
            int sameColorCountUnder = 0;
            if(noneCount >= 2){
                //cout<<"ColorListSize:"<<settableColors.size()<<endl;
                if(settableColors[settableColors.size()-1] == None){
                    settableColors.erase(settableColors.begin()+settableColors.size()-1);
                    //cout<<"Erase None"<<endl;
                }
            }
            int color = rand()%(settableColors.size());
            if(widthCount >= 3){
                //widthCount >= 3からは左側に3個同じ色がある可能性がある
                
                for(int i = 1; i <= 3; i++){
                    if(targetField->GetTileId(widthCount-i, heightCount) == settableColors[color]){
                        sameColorCountSide++;
                    }
                    else{
                        break;
                    }
                }

            }
            if(heightCount <= 7){
                //上から8段目以降は下側に3個同じ色がある可能性がある
                for(int i = 1; i <= 3; i++){
                    if(targetField->GetTileId(widthCount, heightCount+i) == settableColors[color]){
                        sameColorCountUnder++;
                    }
                    else{
                        break;
                    }
                }
            }
            cout<<"Same Side: "<<sameColorCountSide<<", Same Under: "<<sameColorCountUnder<<endl;
            cout<<"Same Color is "<<settableColors[color]<<endl;
            if(sameColorCountSide < 3 && sameColorCountUnder < 3){
                //同じ色が3個に満たない場合は設置して次に進む
                //cout<<widthCount<<endl;
                //cout<<"Enable to Set "<<color<<endl;
                targetField->SetTileId(widthCount, heightCount, settableColors[color]);
                if(settableColors[color] == None){
                    noneCount++;
                }
                widthCount++;
                
            }else{
                settableColors.erase(settableColors.begin()+color);
                cout<<"Left Color"<<endl;
                for(int num : settableColors){
                    cout<<num<<", ";
                }
                cout<<endl;
                //cout<<"Failed to Set"<<endl;
            }
        }
        ///</while(widthCount < defaultFieldWidth)>
        heightCount--;
    }
    ///</while(heightCount >= 0)>
}

int FieldManager::CountSameColors(int direction, vec2 pos, int colorCount, int baseColor){
    int result = colorCount;
    //cout<<"Current Look... "<<pos.x<<", "<<pos.y<<endl;
    vec2 targetPos;
    if(direction == Up){
        targetPos = pos+vec2(0,-1);
    }
    else if(direction == Down){
        targetPos = pos+vec2(0,1);
    }
    else if(direction == Right){
        targetPos = pos+vec2(1,0);
    }
    else if(direction == Left){
        targetPos = pos+vec2(-1,0);
    }
    else{
        return result;
    }
    int targetColor = targetField->GetTileId((int)targetPos.x, (int)targetPos.y);
    bool b = IsSameColor(baseColor, targetColor);
    if(b){
        return CountSameColors(direction, targetPos, result+1, baseColor);
    }
    //cout<<"Result... "<<result<<endl;
    return result;
}
