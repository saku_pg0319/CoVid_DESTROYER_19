//
//  FieldManager.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/29.
//

#ifndef FieldManager_hpp
#define FieldManager_hpp

#include <stdio.h>
#include "Field.hpp"
#include "Score.hpp"
#include "Actor.hpp"
#include "SeedGenerator.hpp"
#include "NextBlocks.hpp"
#include "GameConditionText.hpp"
#include "GameSprashText.hpp"

enum directionNum{
    Up, Down, Right, Left
};

class FieldManager : public Actor{
public:
    FieldManager();
    void SetUp() override;
    void Update() override;
    void KeyPressed(int keycode) override;
    bool CheckClearableTile();
private:
    Field *targetField;
    Score *targetScoreText;
    NextBlocks *targetNexts;
    GameCondition *targetGC;
    SprashText *targetSpT0;
    
    bool isDroppingBlock = false;
    //void UpdateFieldCondition();
    
    void WriteFieldCondition();
    bool IsSameColor(int baseColorId, int targetColorId);
    void DropBlock();
    
    int chainCount = 1;
    int CountSameColors(int direction, vec2 pos, int colorCount, int baseColor);
    
    int controllableSwitchFrame = 0;
    int maxControllableSwitchFrame = 60;
    
    bool isFallingStarted = false;
    DroppingBlockList *dbl;
    
    bool isGameOvered = false;
    bool isCleared = false;
    int startLevel;
    int startFrame = 0;
    int maxStartFrame = 60;
    float fadeLevel = 0;
    void GenerateField();
    void GenerateEasy();
};

#endif /* FieldManager_hpp */
