//
//  Controller.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/27.
//

#include "Controller.hpp"

void FieldController::SetUp(){
    Actor::SetUp();
    Field *field = (Field *)GetOtherGameObj("Field");
    targetField = field;
    fManager = (FieldManager *)GetOtherGameObj("FieldManager");
}

void FieldController::Update(){
    
}

void FieldController::KeyPressed(int keycode){
    if(keycode == 'o'){
        bool f = targetField->GetIsFalling();
        bool d = targetField->GetIsDropping();
        bool s = targetField->GetIsSpawnedDropping();
        cout<<"Fall: "<<f<<", Drop: "<<d<<", Spawned: "<<s<<endl;
    }
    
    if(keycode == 'p'){
//        targetField->SetTileId(2, 0, RedDropping);
//        targetField->SetTileId(3, 0, BlueDropping);
//        cout<<"Controller: Set Block"<<endl;
        //targetField->ToggleIsSpawnedDropping();
        //fManager->CheckClearableTile();
    }
    if(keycode == 'a'){
        targetField->MoveHorizontal(false);
    }
    else if(keycode == 'd'){
        targetField->MoveHorizontal(true);
    }
    
    if(keycode == 's'){
        targetField->AddDroppingFrame(60);
    }
    
    if(keycode == 'k'){
        targetField->RotateDroppedBlocks(true);
    }
    else if(keycode == 'j'){
        targetField->RotateDroppedBlocks(false);
    }
}
