//
//  Controller.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/27.
//

#ifndef Controller_hpp
#define Controller_hpp

#include <stdio.h>
#include "Actor.hpp"
#include "Field.hpp"
#include "FieldManager.hpp"


///フィールドの操作をする
class FieldController : public Actor{
private:
    Field *targetField;
    FieldManager *fManager;
public:
    void SetUp() override;
    void Update() override;
    void KeyPressed(int keycode) override;
};


#endif /* Controller_hpp */
