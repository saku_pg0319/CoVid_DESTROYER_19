//
//  SeedGenerator.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/03.
//

#include "SeedGenerator.hpp"

DroppingBlockList::DroppingBlockList(){
    srand(time(nullptr));
    for(int i = 0; i < 60; i++){
        vec2 v = vec2(RedDropping+rand()%3, RedDropping+rand()%3);
        //cout<<"Block: "<<v.x<<", "<<v.y<<endl;
        blockList.push_back(v);
    }
    dropCount = 0;
}

vec2 DroppingBlockList::GetNextBlocks(bool advanceCount){
    if(advanceCount){
        dropCount++;
        if(dropCount == 59){
            dropCount = 0;
            return blockList[59];
        }
        return blockList[dropCount-1];
    }else{
        return blockList[dropCount];
    }
}
