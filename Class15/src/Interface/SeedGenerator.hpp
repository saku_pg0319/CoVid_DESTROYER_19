//
//  SeedGenerator.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/03.
//

#ifndef SeedGenerator_hpp
#define SeedGenerator_hpp

#include <stdio.h>
#include "ofApp.h"
#include <random>
#include "Instance.h"

class DroppingBlockList{
private:
    vector<vec2> blockList;
    int dropCount;
public:
    DroppingBlockList();
    vec2 GetNextBlocks(bool advanceCount);
};

#endif /* SeedGenerator_hpp */
