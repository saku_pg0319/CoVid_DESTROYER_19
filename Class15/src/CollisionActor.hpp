//
//  CollisionActor.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/13.
//

#ifndef CollisionActor_hpp
#define CollisionActor_hpp

#include <stdio.h>
#include "Actor.hpp"
#include "Physics.hpp"

//衝突判定を受け付けるActorクラス
//物理挙動に関してはPhysicsの設定を変更する

class CollisionActor : public Actor{
protected:

public:
    CollisionActor();
    Physics physics;

    virtual void SetUp() override;
    virtual void Update() override;
    virtual void Draw() override;
    virtual void KeyPressed(int keycode) override;
    virtual void KeyReleased(int keycode) override;
    virtual void MouseMoved(int x, int y) override;
    virtual void MousePressed(int x, int y, int button) override;
    virtual void MouseReleased(int x, int y, int button) override;
    virtual void MouseEntered(int x, int y) override;
    virtual void MouseExited(int x, int y) override;
};

#endif /* CollisionActor_hpp */
