#pragma once

#include "ofMain.h"
#include "Instance.h"

using namespace glm;

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
        ofTrueTypeFont textFont;
};

void LoadScene(int nextSceneNum);
void LoadSceneWithDestroy(int nextSceneNum);
void ReloadScene();
void SetOriginFont(string fontName, float fontSize);
void Instantiate(vec3 position, float rotation, intptr_t objectptr);
void SetDifficulty(int _difficulty);
int GetDifficulty();
void SetCurrentScore(string name, int score);
vector<PlayerScore> GetScoreList();
PlayerScore GetCurrentScore();
void SetScoreList(vector<PlayerScore> vps);
intptr_t GetCurrentLoadScene();
intptr_t GetOtherActor(string objName);
intptr_t GetOtherGameObj(string objName);
intptr_t GetOtherText(string objName);
//ofTrueTypeFont f;
