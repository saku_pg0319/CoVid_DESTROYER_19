//
//  SampleScene1.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/12.
//

#ifndef SampleScene1_hpp
#define SampleScene1_hpp

#include <stdio.h>
#include "Scene.hpp"
#include "PhysicalActorSample.hpp"

class SampleScene1 : public Scene{
public:
    SampleScene1(); //Set Object List
    void SetUp() override; //Called first frame
    void Update() override; //Called per frames
    void Draw() override; //Called after Update()
    void KeyPressed(int keycode) override;
};

#endif /* SampleScene1_hpp */
