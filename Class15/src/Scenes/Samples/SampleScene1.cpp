//
//  SampleScene1.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/12.
//

#include "SampleScene1.hpp"

SampleScene1::SampleScene1() : Scene(){
    PhysicalActorSample *pas = new PhysicalActorSample;
    actors = {pas};
    name = "SampleScene1";
}

void SampleScene1::SetUp(){
    Scene::SetUp();
    
}

void SampleScene1::Update(){
    Scene::Update();
    
}

void SampleScene1::Draw(){
    ofBackground(255, 255, 255);
    Scene::Draw();
    
}

void SampleScene1::KeyPressed(int keycode){
    Scene::KeyPressed(keycode);
}
