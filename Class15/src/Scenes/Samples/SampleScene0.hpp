//
//  SampleScene0.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/12.
//

#ifndef SampleScene0_hpp
#define SampleScene0_hpp

#include <stdio.h>
#include "Scene.hpp"
#include "TestObj01.hpp"
#include "SampleText.hpp"

class SampleScene0 : public Scene{
public:
    SampleScene0(); //Set Object List
    void SetUp() override; //Called first frame
    void Update() override; //Called per frames
    void Draw() override; //Called after Update()
    void KeyPressed(int keycode) override;
private:
    void ShowActorCount();
};

#endif /* SampleScene0_hpp */
