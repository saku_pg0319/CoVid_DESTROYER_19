//
//  SampleScene0.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/12.
//

#include "SampleScene0.hpp"

SampleScene0::SampleScene0() : Scene(){
    TestObj01 *test01 = new TestObj01();
    SampleText *sampleText = new SampleText();
    actors = {test01};
    texts = {sampleText};
    name = "SampleScene0";
    
}

void SampleScene0::SetUp(){
    Scene::SetUp();
    
}

void SampleScene0::Update(){
    Scene::Update();
    
}

void SampleScene0::Draw(){
    ofBackground(0, 0, 0);
    Scene::Draw();
    
}

void SampleScene0::KeyPressed(int keycode){
    Scene::KeyPressed(keycode);
    if(keycode == 'f'){
        TestObj01 *another = new TestObj01();
        another->transform.position += vec2(200, 0);
        this->AddActor(another);
        cout<<"Add"<<endl;
        
    }
    if(keycode == 'c'){
        ShowActorCount();
    }
}

void SampleScene0::ShowActorCount(){
    cout<<actors.size()<<endl;
}
