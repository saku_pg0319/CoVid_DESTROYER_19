//
//  Results.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#ifndef Results_hpp
#define Results_hpp

#include <stdio.h>
#include "Scene.hpp"
#include "CursorText.hpp"
#include "GameSprashText.hpp"

class Results : public Scene{
public:
    Results(); //Set Object List
    void SetUp() override; //Called first frame
    void Update() override; //Called per frames
    void Draw() override; //Called after Update()
    void KeyPressed(int keycode) override;
    void KeyReleased(int keycode) override;
private:
    CursorText *firstChar;
    CursorText *secondChar;
    CursorText *lastChar;
    CursorText *scoreText;
    vec2 cursorPos;
    int cursorCount = 0;
    int stringInterval = 40;
    int stringIntervalToScore = 60;
    char characters[3];
    int score;
    int countFrame = 0;
    bool setScore = false;
    void SetSortedScoreList();
};

#endif /* Results_hpp */
