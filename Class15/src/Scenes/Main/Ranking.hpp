//
//  Ranking.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/07.
//

#ifndef Ranking_hpp
#define Ranking_hpp

#include <stdio.h>
#include "Scene.hpp"
#include "ScoreList.hpp"
#include "GameSprashText.hpp"

class Ranking : public Scene{
public:
    Ranking();
    void SetUp() override;
    void Update() override;
    void KeyPressed(int keycode) override;
private:
};

#endif /* Ranking_hpp */
