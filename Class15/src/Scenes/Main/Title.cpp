//
//  Title.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#include "Title.hpp"

Title::Title() : Scene(){
    TitleText *tt = new TitleText();
    SprashText *spT0 = new SprashText();
    spT0->text = "2023.01.13 SHUTA OKUNO";
    SprashText *spT1 = new SprashText();
    spT1->text = "W/S/A/D:Move Cursor\nSpace:Select Mode";
    spT1->SetFont("k8x12S.ttf", 30);
    spT1->transform.position = vec2(20, 600);
    texts = {tt, spT0, spT1};
    name = "Title";
}

void Title::SetUp(){
    Scene::SetUp();
    texts[1]->transform.position = vec2(10, ofGetHeight()-25);
    CursorText *text0 = new CursorText("EASY MODE", vec2(ofGetWidth()/2, ofGetHeight()/2+100));
    CursorText *text1 = new CursorText("MEDIUM MODE", vec2(ofGetWidth()/2, ofGetHeight()/2+150));
    CursorText *text2 = new CursorText("HARD MODE", vec2(ofGetWidth()/2, ofGetHeight()/2+200));
    CursorText *text3 = new CursorText("CUSTOM MODE:   ", vec2(ofGetWidth()/2+20, ofGetHeight()/2+250));
    CursorText *text4 = new CursorText("RESULTS", vec2(ofGetWidth()/2, ofGetHeight()/2+300));
    
    custom = text3;
    texts.push_back(text0);
    texts.push_back(text1);
    texts.push_back(text2);
    texts.push_back(custom);
    texts.push_back(text4);
    startFrame = 0;
    isGameStart = false;
    for(Text *text : texts){
        if(text->GetName() != "TitleText"){
            text->SetUp();
        }
    }
    text4->color = vec4(100, 255, 100, 255);
}

void Title::Update(){
    //cout<<"PPPPPP"<<endl;
    Scene::Update();
}

void Title::Draw(){
    Scene::Draw();
    ofSetColor(255, 255, 255);
    ofNoFill();
    ofDrawRectangle(ofGetWidth()/2-200, ofGetHeight()/2+100-12+50*selector, 400, 30);
    if(isGameStart){
        if(selector == 4){
            LoadSceneWithDestroy(3);
        }
        else{
            startFrame++;
            //cout<<(float)(255/maxStartFrame*startFrame)<<endl;
            ofSetColor(0, 0, 0,(float)(255/maxStartFrame*startFrame));
            ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
            if(startFrame>=maxStartFrame){
                if(selector != 3){
                    SetDifficulty(selector*2+2);
                    cout<<"Selector"<<endl;
                }
                else{
                    SetDifficulty(difficulty);
                    //Difficulty = 8-difficulty;
                }
                LoadSceneWithDestroy(1);
            }
        }
        
    }
    //ofDrawCircle(ofGetWidth()/2-200, ofGetHeight()/2+100+50*difficulty, 5);
}

void Title::KeyPressed(int keycode){
    Scene::KeyPressed(keycode);
    cout<<keycode<<endl;
    if(keycode == 'w' || keycode == 57357){
        selector--;
        if(selector < 0){
            selector = 4;
        }
    }
    else if(keycode == 's' || keycode == 57359){
        selector++;
        if(selector > 4){
            selector = 0;
        }
    }
    if(selector == 3){
        if(keycode == 'a'){
            difficulty-=1;
            if(difficulty <= 0){
                difficulty = 7;
            }
        }
        else if(keycode == 'd'){
            difficulty++;
            if(difficulty > 7){
                difficulty = 1;
            }
        }
        string levelText = to_string(difficulty);
        string s = "";
        if(levelText.length() < 2){
            while(levelText.length() + s.length() < 2){
                s+="0";
            }
        }
        custom->text = "CUSTOM MODE: "+s+levelText;
        
    }else{
        custom->text = "CUSTOM MODE:   ";
    }
    
    if(keycode == 32){
        isGameStart = true;
    }
}

void Title::KeyReleased(int keycode){
    Scene::KeyReleased(keycode);
}
