//
//  Title.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#ifndef Title_hpp
#define Title_hpp

#include <stdio.h>
#include "Scene.hpp"
#include "TitleText.hpp"
#include "CursorText.hpp"
#include "GameSprashText.hpp"

class Title : public Scene{
public:
    Title(); //Set Object List
    void SetUp() override; //Called first frame
    void Update() override; //Called per frames
    void Draw() override; //Called after Update()
    void KeyPressed(int keycode) override;
    void KeyReleased(int keycode) override;
private:
    CursorText *custom;
    int selector = 1; //0:easy, 1:medium, 2:hard, 3:custom
    int difficulty = 5;
    bool isGameStart;
    int startFrame;
    int maxStartFrame = 40;
};

#endif /* Title_hpp */
