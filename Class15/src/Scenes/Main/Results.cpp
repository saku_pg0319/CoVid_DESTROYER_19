//
//  Results.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#include "Results.hpp"

Results::Results() : Scene(){
    strcpy(characters, "AAA");
    SprashText *spT0 = new SprashText();
    spT0->SetFont("k8x12S.ttf", 50);
    spT0->text = "RESULT";
    SprashText *spT1 = new SprashText();
    spT1->text = "Press\n   W/S:Change Character,\n   A/D:Move Cursor\n   Space:End and Back to Title";
    spT1->transform.position = vec2(20, 100);
    spT1->SetFont("k8x12S.ttf", 25);
    texts = {spT0, spT1};
}

void Results::SetUp(){
    Scene::SetUp();
    texts[0]->transform.position = vec2(ofGetWidth()/2-texts[0]->GetTextWidth()/3*2,335);
    //texts[1]->SetFont("k8x12S.ttf", 10);
    //cout<<"Result"<<endl;
    vec2 anchorPos = vec2(ofGetWidth()/2, 400);
    firstChar = new CursorText("A", anchorPos);
    secondChar = new CursorText("A", anchorPos+vec2(stringInterval, 0));
    lastChar = new CursorText("A", anchorPos+vec2(stringInterval*2, 0));
    scoreText = new CursorText("00000000", anchorPos+vec2(stringInterval*3, 0));
    texts.push_back(firstChar);
    texts.push_back(secondChar);
    texts.push_back(lastChar);
    texts.push_back(scoreText);
    for(Text *text : texts){
        text->SetUp();
        if(text->GetName() != "GameSprash"){
            text->SetFont("k8x12S.ttf", 50);
        }
    }
    scoreText->transform.position+=vec2(scoreText->GetTextWidth()/2,0);
    float distance = firstChar->GetTextWidth()*3+stringInterval*2+stringIntervalToScore+scoreText->GetTextWidth();
    for(Text *text : texts){
        if(text->GetName() != "GameSprash"){
            text->transform.position -= vec2(distance/2,0);
        }
    }
    cursorPos = firstChar->transform.position-vec2(10,firstChar->GetTextHeight()+10-5);
    score = GetCurrentScore().score;
    string _scoreText = to_string(score);
    string s = "";
    if(_scoreText.length() < 8){
        while(_scoreText.length() + s.length() < 8){
            s+="0";
        }
    }
    scoreText->text = s+_scoreText;
//    cout<<firstChar->text<<secondChar->text<<lastChar->text<<": "<<score<<endl;
//    cout<<firstChar->transform.position.x<<endl;
    //cursorPos = anchorPos;
}

void Results::Update(){
    Scene::Update();
    firstChar->text = {characters[0]};
    secondChar->text = {characters[1]};
    lastChar->text = {characters[2]};
    cursorPos = firstChar->transform.position-vec2(10,firstChar->GetTextHeight()+10-5) + vec2(stringInterval,0)*cursorCount;
    //cout<<firstChar->text<<secondChar->text<<lastChar->text<<": "<<score<<endl;
}

void Results::Draw(){
    Scene::Draw();
    ofNoFill();
    ofSetLineWidth(2);
    ofSetColor(255, 255, 255);
    //cout<<cursorPos.x<<", "<<cursorPos.y<<", "<<firstChar->GetTextWidth()<<", "<< firstChar->GetTextHeight()<<endl;
    ofDrawRectangle(cursorPos.x, cursorPos.y, firstChar->GetTextWidth()+20, firstChar->GetTextHeight()+20);
    ofFill();
    ofDrawTriangle(cursorPos.x+firstChar->GetTextWidth()/2+10, cursorPos.y-10, cursorPos.x+firstChar->GetTextWidth()/2+15, cursorPos.y-5, cursorPos.x+firstChar->GetTextWidth()/2+5, cursorPos.y-5);
    ofDrawTriangle(cursorPos.x+firstChar->GetTextWidth()/2+10, cursorPos.y+firstChar->GetTextHeight()+20+10, cursorPos.x+firstChar->GetTextWidth()/2+15, cursorPos.y+firstChar->GetTextHeight()+20+5, cursorPos.x+firstChar->GetTextWidth()/2+5, cursorPos.y+firstChar->GetTextHeight()+20+5);
    
}

void Results::KeyPressed(int keycode){
    Scene::KeyPressed(keycode);
    //cout<<cursorPos.x<<", "<<cursorPos.y<<", "<<firstChar->GetTextWidth()<<", "<< firstChar->GetTextHeight()<<endl;
    if(keycode == 'd'){
        cursorCount++;
        if(cursorCount > 2){
            cursorCount = 0;
        }
    }else if(keycode == 'a'){
        cursorCount--;
        if(cursorCount < 0){
            cursorCount = 2;
        }
    }
    if(keycode == 'w'){
        characters[cursorCount]++;
        if(characters[cursorCount] > 'Z'){
            //cout<<"Move"<<endl;
            characters[cursorCount] = 'A';
        }
        //cout<<characters[cursorCount]<<endl;
    }
    else if(keycode == 's'){
        characters[cursorCount]--;
        if(characters[cursorCount] < 'A'){
            //cout<<"Move"<<endl;
            characters[cursorCount] = 'Z';
        }
        //cout<<characters[cursorCount]<<endl;
    }
    
    if(keycode == 32){
        string str(characters,3);
        SetCurrentScore(str, score);
        setScore = true;
        SetSortedScoreList();
        LoadSceneWithDestroy(0);
    }
}

void Results::KeyReleased(int keycode){
    Scene::KeyReleased(keycode);
}

void Results::SetSortedScoreList(){
    vector<PlayerScore> vps = GetScoreList();
    vps.push_back(GetCurrentScore());
    cout<<"Sorting: "<<vps.size()<<endl;
    for(int i = 0; i < vps.size()-1; i++){
        for(int j = 0; j < vps.size()-1; j++){
            cout<<i<<", "<<j<<endl;
            if(vps[j].score < vps[j+1].score){
                PlayerScore tmp = vps[j];
                vps[j] = vps[j+1];
                vps[j+1] = tmp;
            }
        }
    }
    for(PlayerScore ps : vps){
        cout<<ps.score<<endl;
    }
    vps.pop_back();
    SetScoreList(vps);
}
