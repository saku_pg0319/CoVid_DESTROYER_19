//
//  PreStage.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/29.
//

#ifndef PreStage_hpp
#define PreStage_hpp

#include <stdio.h>
#include "Scene.hpp"
#include "Field.hpp"

#include "Score.hpp"
#include "Next.hpp"
#include "GameConditionText.hpp"
#include "HowToPlayText.hpp"
#include "GameSprashText.hpp"

#include "FieldManager.hpp"
#include "Controller.hpp"
#include "NextBlocks.hpp"

class PreStage : public Scene{
public:
    PreStage(); //Set Object List
    void SetUp() override; //Called first frame
    void Update() override; //Called per frames
    void Draw() override; //Called after Update()
    void KeyPressed(int keycode) override;
    void KeyReleased(int keycode) override;
private:
    

};

#endif /* PreStage_hpp */
