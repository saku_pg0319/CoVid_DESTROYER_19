//
//  PreStage.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/29.
//

#include "PreStage.hpp"

PreStage::PreStage() : Scene(){
    Field *field = new Field();
    Score *score = new Score();
    NextText *next = new NextText();
    GameCondition *gc = new GameCondition();
    HowToPlay *htp = new HowToPlay();
    NextBlocks *nextBlocks = new NextBlocks();
    FieldManager *fManager = new FieldManager();
    FieldController *controller = new FieldController();
    SprashText *st = new SprashText();
    gameObjects = {field, nextBlocks};
    actors = {fManager, controller};
    texts = {score, next, gc, htp, st};
    name = "PreStage";
    
}

void PreStage::SetUp(){
    Scene::SetUp();
}

void PreStage::Update(){
    Scene::Update();
}

void PreStage::Draw(){
    Scene::Draw();
}

void PreStage::KeyPressed(int keycode){
    Scene::KeyPressed(keycode);
}

void PreStage::KeyReleased(int keycode){
    Scene::KeyReleased(keycode);
}


