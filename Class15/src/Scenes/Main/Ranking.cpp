//
//  Ranking.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/07.
//

#include "Ranking.hpp"

Ranking::Ranking(){
    ScoreList *scoreList = new ScoreList();
    SprashText *keyAnnounce = new SprashText();
    keyAnnounce->text = "Press 'Space' \nto Back Title";
    keyAnnounce->transform.position = vec2(50,100);
    gameObjects = {scoreList};
    texts = {keyAnnounce};
    name = "Ranking";
}

void Ranking::SetUp(){
    Scene::SetUp();
}

void Ranking::Update(){
    Scene::Update();
}

void Ranking::KeyPressed(int keycode){
    Scene::KeyPressed(keycode);
    if(keycode == 32){
        LoadSceneWithDestroy(0);
    }
}
