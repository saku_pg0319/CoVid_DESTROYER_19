//
//  ScoreList.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#ifndef ScoreList_hpp
#define ScoreList_hpp

#include <stdio.h>
#include "GameObject.hpp"
#include "GameSprashText.hpp"

class ScoreList : public GameObject{
public:
    ScoreList();
    void SetUp() override;
    void Draw() override;
private:
    vector<SprashText *> spTs;
    
};

#endif /* ScoreList_hpp */
