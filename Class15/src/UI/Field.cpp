//
//  Field.cpp
//  Class15
//
//  Created by hw20a029 on 2022/12/23.
//

#include "Field.hpp"

Field::Field() : GameObject(){
    tileSize = defaultTileSize;
    transform.position = vec2(1024/2-60*3, 50);
    name = "Field";
    //cout<<"Field: Diff"<<GetDifficulty()<<endl;
    backgroundColor = vec4(255, 255, 255, 50);
}

void Field::Draw(){
    ///<固定されたブロックの描画>
    DrawSolid();
    ///</固定されたブロックの描画>
    ///<落下中ブロックの描画>
    DrawFalling();
    ///</落下中のブロックの描画>
    ///<Drop操作中のブロックの描画>
    DrawDropping();
    ///</Drop操作中のブロックの描画>
    ///<グリッドの描画>
    ofNoFill();
    ofSetLineWidth(2);
    ofSetColor(255, 255, 255);
    for(int i = 0; i < extent<decltype(fieldIds), 0>::value; i++){
        for(int j = 0; j < extent<decltype(fieldIds), 1>::value; j++){
            ofDrawRectangle(transform.position.x+i*tileSize, transform.position.y+j*tileSize, tileSize, tileSize);
        }
    }
    ofSetLineWidth(1);
    ///</グリッドの描画>
}

void Field::SetTileId(int x, int y, int tileId){
    fieldIds[x][y] = tileId;
}

int Field::GetTileId(int x, int y){
    return fieldIds[x][y];
}

void Field::AddFallingBlockCondition(int x, int y, int tileId){
    fallingBlockConditions.push_back({(float)x, (float)y, (float)tileId});
//    cout<<"Field: Current Falling Block was Added... "<<x<<", "<<y<<", "<<tileId<<endl;
//    cout<<"Field: Current Falling Block List Size... "<<fallingBlockConditions.size()<<endl;
}

vector<vec3> Field::SetFallingBlockConditions(){
    vector<vec3> results;
    for(int k = 0; k < fallingBlockConditions.size(); k++){
        //cout<<"Field: Current Look at... ("<<fallingBlockConditions[k][0]<<", "<<fallingBlockConditions[k][1]<<", "<<fallingBlockConditions[k][2]<<")"<<endl;
        if(fallingFrame >= maxFallingFrame){
            int num = fieldIds[(int)(fallingBlockConditions[k][0])][(int)fallingBlockConditions[k][1]+1];
            bool b0 = num == None && (int)(fallingBlockConditions[k][1]+1) < fieldHeight;
            bool b1 = num >= RedFalling && num <= GreenFalling;
            bool b2 = num >= RedBlock && num <= GreenBlock;
            
            if((b0 || b1 )&& !b2){
                //cout<<"PPPP"<<k<<endl;
                fallingBlockConditions[k][1] += 0.25f;
                results.push_back(vec3(fallingBlockConditions[k][0], fallingBlockConditions[k][1], fallingBlockConditions[k][2]));
                //cout<<"Falling Pos: ("<<(transform.position.x+fallingBlockConditions[k][0])*tileSize<<", "<<(transform.position.y+fallingBlockConditions[k][1])*tileSize<<")"<<endl;
            }///</if(GetTileId((int)(fallingBlockConditions[k][0]), (int)(fallingBlockConditions[k][1]+1)) == None && (int)(fallingBlockConditions[k][1]+1) < fieldHeight)>
            else{
                int color;
                switch((int)fallingBlockConditions[k][2]){
                    case RedFalling:
                        color = RedBlock;
                        break;
                    case BlueFalling:
                        color = BlueBlock;
                        break;
                    case GreenFalling:
                        color = GreenBlock;
                        break;
                    default:
                        color = None;
                        break;
                }
                //fallingBlockConditions[k][1] -= 0.25f;
                if(fieldIds[(int)(fallingBlockConditions[k][0])][(int)(fallingBlockConditions[k][1])] == None){
                    fieldIds[(int)(fallingBlockConditions[k][0])][(int)(fallingBlockConditions[k][1])] = color;
                }else{
                    fieldIds[(int)(fallingBlockConditions[k][0])][(int)(fallingBlockConditions[k][1])-1] = color;
                }
                
                //cout<<"Field: Found Floor"<<endl;
                //cout<<"Field: Erace at... ("<<fallingBlockConditions[k][0]<<", "<<fallingBlockConditions[k][1]<<", "<<fallingBlockConditions[k][2]<<")"<<endl;
                fallingBlockConditions.erase(fallingBlockConditions.begin()+k);
                //cout<<"Field: Current Size... "<<fallingBlockConditions.size()<<endl;
            }///</else>
            if(k == fallingBlockConditions.size()-1){
                fallingFrame = 0;
            }
        }///</if(fallingFrame >= maxFallingFrame)>
        else{
            results.push_back(vec3(fallingBlockConditions[k][0], fallingBlockConditions[k][1], fallingBlockConditions[k][2]));
        }///</else>
    }
    return results;
    
}

bool Field::GetIsFalling(){
    return isFalling;
}

bool Field::ToggleIsFalling(){
    isFalling = !isFalling;
    return isFalling;
}

void Field::DrawSolid(){
    ofFill();
    bool isVirus;
    for(int i = 0; i < extent<decltype(fieldIds), 0>::value; i++){
        for(int j = 0; j < extent<decltype(fieldIds), 1>::value; j++){
            ofSetColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
            ofDrawRectangle(transform.position.x+i*tileSize, transform.position.y+j*tileSize, tileSize, tileSize);
            switch(fieldIds[i][j]){
                case RedVirus:
                    ofSetColor(255, 0, 0);
                    isVirus = true;
                    break;
                case BlueVirus:
                    ofSetColor(0, 0, 255);
                    isVirus = true;
                    break;
                case GreenVirus:
                    ofSetColor(0, 255, 0);
                    isVirus = true;
                    break;
                case RedBlock:
                    ofSetColor(255, 100, 100);
                    isVirus = false;
                    break;
                case BlueBlock:
                    ofSetColor(100, 100, 255);
                    isVirus = false;
                    break;
                case GreenBlock:
                    ofSetColor(100, 255, 100);
                    isVirus = false;
                    break;
                case Checker:
                    ofSetColor(100, 50, 150);
                    isVirus = true;
                    break;
                case RedFalling:
                    ofSetColor(255, 100, 100);
                    isVirus = false;
                    break;
                case BlueFalling:
                    ofSetColor(100, 100, 255);
                    isVirus = false;
                    break;
                case GreenFalling:
                    ofSetColor(100, 255, 100);
                    isVirus = false;
                    break;
                default:
                    ofSetColor(255, 255, 255, 0);
                    isVirus = false;
                    break;
            }
            if(isVirus){
                ofDrawCircle(transform.position.x+i*tileSize+tileSize/2, transform.position.y+j*tileSize+tileSize/2, tileSize/3);
                ofSetLineWidth(4);
                float X = transform.position.x+i*tileSize+tileSize/2;
                float Y = transform.position.y+j*tileSize+tileSize/2;
                float radius = tileSize/2;
                int i = 0;
                ofDrawLine(X+radius*cos(i*M_PI/6), Y+radius*sin(i*M_PI/6), X+radius*cos((i+6)*M_PI/6), Y+radius*sin((i+6)*M_PI/6));
                i++;
                ofDrawLine(X+radius*cos(i*M_PI/6), Y+radius*sin(i*M_PI/6), X+radius*cos((i+6)*M_PI/6), Y+radius*sin((i+6)*M_PI/6));
                i++;
                ofDrawLine(X+radius*cos(i*M_PI/6), Y+radius*sin(i*M_PI/6), X+radius*cos((i+6)*M_PI/6), Y+radius*sin((i+6)*M_PI/6));
                i++;
                ofDrawLine(X+radius*cos(i*M_PI/6), Y+radius*sin(i*M_PI/6), X+radius*cos((i+6)*M_PI/6), Y+radius*sin((i+6)*M_PI/6));
                i++;
                ofDrawLine(X+radius*cos(i*M_PI/6), Y+radius*sin(i*M_PI/6), X+radius*cos((i+6)*M_PI/6), Y+radius*sin((i+6)*M_PI/6));
                i++;
                ofDrawLine(X+radius*cos(i*M_PI/6), Y+radius*sin(i*M_PI/6), X+radius*cos((i+6)*M_PI/6), Y+radius*sin((i+6)*M_PI/6));
                ofSetColor(0, 0, 0, 100);
                ofFill();
                ofDrawCircle(X+tileSize/6, Y+tileSize/6, tileSize/24);
                ofDrawCircle(X-tileSize/6, Y+tileSize/6, tileSize/24);
                ofDrawCircle(X+tileSize/6, Y-tileSize/6, tileSize/24);
                ofDrawCircle(X-tileSize/6, Y-tileSize/7, tileSize/24);
                ofDrawCircle(X+tileSize/5, Y+tileSize/3, tileSize/24);
                ofDrawCircle(X, Y+tileSize/4, tileSize/24);
                ofDrawCircle(X+tileSize/10, Y, tileSize/24);
                
                
//                ofDrawRectangle(transform.position.x+i*tileSize+tileSize/6, transform.position.y+j*tileSize+tileSize/6, tileSize/3*2, tileSize/3*2);
            }else{
                ofDrawRectangle(transform.position.x+i*tileSize, transform.position.y+j*tileSize, tileSize, tileSize);
            }
            
        }
    }
}

void Field::DrawFalling(){
    ofFill();
    if(fallingBlockConditions.size() != 0){
        isFalling = true;
        fallingFrame++;
        vector<vec3> fallingBlocks = SetFallingBlockConditions();
        ofFill();
        //cout<<"Result Size... "<<fallingBlocks.size()<<endl;
        for(vec3 v : fallingBlocks){
            switch((int)v.z){
                        case RedFalling:
                            ofSetColor(255, 100, 100);
                            break;
                        case BlueFalling:
                            ofSetColor(100, 100, 255);
                            break;
                        case GreenFalling:
                            ofSetColor(100, 255, 100);
                            break;
                        default:
                            break;
                    }
            //cout<<(int)v.z<<endl;
            ofDrawRectangle(transform.position.x+v.x*tileSize, transform.position.y+tileSize*v.y, tileSize, tileSize);
            //cout<<"Draw "<<(int)v.z<<endl;
        }
        
    }
    else{
        isFalling = false;
    }
    
}

void Field::DrawDropping(){
    //操作可能オブジェクトの描画
    //基本はFallingと同じ
    /*
     - isDroppingがtrueのときに実行される(Controllerがランダムにオブジェクトを生成してswitch)
     - droppingFrameを加算し、maxになったら少し落下する
     */
    if(isDropping){
        if(droppedBlocks.size() == 0){
            //cout<<"PPPPP"<<endl;
            //Drop中ブロックを選択
            for(int i = 0; i < extent<decltype(fieldIds), 0>::value; i++){
                for(int j = 0; j < extent<decltype(fieldIds), 1>::value; j++){
                    if(fieldIds[i][j] >= RedDropping && fieldIds[i][j] <= GreenDropping){
                        droppedBlocks.push_back(vec3(i, j, fieldIds[i][j]));
                        fieldIds[i][j] = None;
                    }
                }
            }
            cout<<fieldIds[2][0]<<", "<<fieldIds[3][0]<<endl;
            //cout<<"Field: DroppedBlock was Set"<<endl;
        }///</if(droppedBlocks.size() == 0)>
        else{
            //Drop中のブロックを落下させる
            droppingFrame++;
            //cout<<"Field:dframe... "<<droppingFrame<<endl;
            if(droppingFrame >= maxDroppingFrame){
                //落下フレームに達した時
                //cout<<"Field: Drop"<<endl;
                for(int k = 0; k < droppedBlocks.size(); k++){
                    bool b0 = fieldIds[(int)droppedBlocks[k].x][(int)droppedBlocks[k].y+1] != None;
                    if(b0 || (int)droppedBlocks[k].y+1 == fieldHeight){
                        //下に1つでもブロックがあったらdrop終了
                        //cout<<"Field: Drop was Finished"<<endl;
                        isDropping = false;
                        //drop系タイルをBlockに変換
                        for(int i = 0; i < droppedBlocks.size(); i++){
                            int color;
                            switch((int)droppedBlocks[i].z){
                                case RedDropping:
                                    color = RedBlock;
                                    break;
                                case BlueDropping:
                                    color = BlueBlock;
                                    break;
                                case GreenDropping:
                                    color = GreenBlock;
                                    break;
                                default:
                                    break;
                            }
                            fieldIds[(int)droppedBlocks[i].x][(int)droppedBlocks[i].y] = color;
                            
                        }
                        ///</for(int i = 0; i < droppedBlocks.size(); i++)>
//                            fieldIds[2][0] = None;
//                            fieldIds[3][0] = None;
                        droppedBlocks.clear();
                        return;
                    }
                    droppedBlocks[k].y+=0.25f;
                    //cout<<"Field: Y..."<<v.y<<endl;
                }
                droppingFrame = 0;
            }
            for(vec3 v : droppedBlocks){
                switch((int)v.z){
                    case RedDropping:
                        ofSetColor(255, 100, 100);
                        break;
                    case BlueDropping:
                        ofSetColor(100, 100, 255);
                        break;
                    case GreenDropping:
                        ofSetColor(100, 255, 100);
                        break;
                    default:
                        break;
                }
                ofDrawRectangle(transform.position.x+v.x*tileSize, transform.position.y+tileSize*v.y, tileSize, tileSize);
            }///</for(vec3 v : droppedBlocks)>
            
        }///</else>
    }///</if(isDropping)>
    else{
        
    }///</else>
    
}

bool Field::GetIsDropping(){
    return isDropping;
}
bool Field::ToggleIsDropping(){
    isDropping = !isDropping;
    return isDropping;
}

bool Field::GetIsSpawnedDropping(){
    return isSpawnedDropping;
}
bool Field::ToggleIsSpawnedDropping(){
    isSpawnedDropping = !isSpawnedDropping;
    //cout<<"Spawn Toggled to "<<isSpawnedDropping<<endl;
    return isSpawnedDropping;
}

int Field::AddDroppingFrame(int advanceRate){
    droppingFrame+=advanceRate;
    return droppingFrame;
}

void Field::MoveHorizontal(bool isRight){
    if(isDropping && droppedBlocks.size()>0){
        bool isMovable = false;
        int movedirection = isRight? 1 : -1;
        for(int i = 0; i < droppedBlocks.size(); i++){
            bool b0 = fieldIds[(int)droppedBlocks[i].x+movedirection][(int)droppedBlocks[i].y] == None;
            bool b1 = (int)droppedBlocks[i].x+movedirection >= 0 && (int)droppedBlocks[i].x+movedirection < fieldWidth;
            if(b0 && b1){
                isMovable = true;
            }
            else{
                cout<<"Field: Can't Slide... "<<droppedBlocks[i].z<<endl;
                isMovable = false;
                break;
            }
        }
        if(isMovable){
            for(int i = 0; i < droppedBlocks.size(); i++){
                droppedBlocks[i].x+=movedirection;
            }
        }
    }
}

    
void Field::RotateDroppedBlocks(bool rotateRight){
    //Drop操作中のブロックをリストの0番を軸にして90度回転する
    if(isDropping && droppedBlocks.size() != 0){
        //cout<<"Rotate Start"<<endl;
        int rotateDirection = rotateRight? -1 : 1;
        vec2 positionDiffs = vec2((int)(droppedBlocks[1].x-droppedBlocks[0].x), (int)(droppedBlocks[1].y-droppedBlocks[0].y));
        vec2 _tmp = vec2(droppedBlocks[1].x, droppedBlocks[1].y);
        int currentDirection = -1; //0:Up,1:Down,2:Right,3:Left
        if(positionDiffs.x == 0 && positionDiffs.y == -1){
            //上向きのとき
            //cout<<"Upto"<<endl;
            _tmp.x -= rotateDirection;
            _tmp.y++;
            currentDirection = 0;
        }
        else if(positionDiffs.x == 0 && positionDiffs.y == 1){
            //下向きのとき
            //cout<<"Lowto"<<endl;
            _tmp.x += rotateDirection;
            _tmp.y--;
            currentDirection = 1;
        }
        else if(positionDiffs.x == 1 && positionDiffs.y == 0){
            //右向きのとき
            _tmp.x--;
            _tmp.y -= rotateDirection;
            currentDirection = 2;
        }
        else if(positionDiffs.x == -1 && positionDiffs.y == 0){
            //左向きのとき
            _tmp.x++;
            _tmp.y += rotateDirection;
            currentDirection = 3;
        }else{
            cout<<"Error"<<endl;
        }
        
        bool b0 = fieldIds[(int)_tmp.x][(int)ceil(_tmp.y)] == None; //移動先がNone
        bool b1 = _tmp.x>=0 && _tmp.x < fieldWidth; //移動先が壁
        bool b2 = _tmp.y>=0 && _tmp.y < fieldHeight; //移動先が床(天井)
        bool b3 = droppedBlocks[0].y < fieldHeight;
        bool b4 =  fieldIds[(int)droppedBlocks[0].x][(int)ceil(droppedBlocks[0].y+1)] == None;
        bool b5R = (droppedBlocks[0].x < fieldWidth-1)? fieldIds[(int)droppedBlocks[0].x+1][(int)droppedBlocks[0].y] == None : false;
        bool b5L = (droppedBlocks[0].x > 0)? fieldIds[(int)droppedBlocks[0].x-1][(int)droppedBlocks[0].y] == None : false;
        cout<<"IsWall?... Right: "<<b5R<<", Left: "<<b5L<<endl;
        if(b0 && b1 && b2){
            droppedBlocks[1].x = _tmp.x;
            droppedBlocks[1].y = _tmp.y;
        }
        else{
            if((currentDirection == 0 || currentDirection == 1) && b5R){
                int horizontalDirection = (currentDirection == 0)? 1 : -1;
                droppedBlocks[0].x +=rotateDirection*horizontalDirection;
                droppedBlocks[1].x = _tmp.x+rotateDirection*horizontalDirection;
                droppedBlocks[1].y = _tmp.y;
            }
            else if((currentDirection == 2 || currentDirection == 3) && !(b3&&b4)){
                droppedBlocks[0].y--;
                droppedBlocks[1].x = _tmp.x;
                droppedBlocks[1].y = _tmp.y-1;
            }
            
            if(!b5R && !b5L){
                vec2 __tmp = vec2(droppedBlocks[0].x, droppedBlocks[0].y);
                droppedBlocks[0].x = droppedBlocks[1].x;
                droppedBlocks[0].y = droppedBlocks[1].y;
                droppedBlocks[1].x = __tmp.x;
                droppedBlocks[1].y = __tmp.y;
            }
        }
    }
    ///</if(isDropping && droppedBlocks.size() != 0)>
}

int Field::GetCurrentFallingFrame(){
    return fallingFrame;
}

int Field::GetVirusCount(){
    int c = 0;
    for(int i = 0; i < fieldWidth; i++){
        for(int j = 0; j < fieldHeight; j++){
            if(fieldIds[i][j] >= RedVirus && fieldIds[i][j] <= GreenVirus){
                c++;
            }
        }
    }
    return c;
}

void Field::SetDroppingFrame(int _droppingFrameDiff){
    maxDroppingFrame = maxDroppingFrame - _droppingFrameDiff;
}
