//
//  ScoreList.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/04.
//

#include "ScoreList.hpp"

ScoreList::ScoreList() : GameObject(){
}

void ScoreList::SetUp(){
    GameObject::SetUp();
    vector<PlayerScore> vps = GetScoreList();
    cout<<"Got Score List"<<endl;
    for(int i = 0; i < 10; i++){
        string rankText = to_string(i+1);
        string s0 = "";
        if(rankText.length() < 2){
            while(rankText.length() + s0.length() < 2){
                s0+="0";
            }
        }
        string scoreText = to_string(vps[i].score);
        string s1 = "";
        if(scoreText.length() < 8){
            while(scoreText.length() + s1.length() < 8){
                s1+="0";
            }
        }
        SprashText *spT0 = new SprashText();
        spT0->text = s0+rankText+": "+vps[i].name+" "+s1+scoreText;
        cout<<s0+rankText+": "+vps[i].name+" "+s1+scoreText<<endl;
        spT0->color = vec4(255, 255, 255, 255);
        spT0->SetFont("k8x12S.ttf", 30);
        float X = spT0->GetTextWidth();
        //cout<<X<<endl;
        spT0->transform.position = vec2(1024/2-X/2,100+60*i);
        spTs.push_back(spT0);
    }
    SprashText *spTc = new SprashText();
    PlayerScore prevScore = GetCurrentScore();
    string scoreText = to_string(prevScore.score);
    string s = "";
    if(scoreText.length() < 8){
        while(scoreText.length() + s.length() < 8){
            s+="0";
        }
    }
    spTc->text = "Previous: "+prevScore.name+" "+s+scoreText;
    float X = spTs[0]->GetTextWidth();
    spTc->transform.position = vec2(ofGetWidth()/2-X/7*6-6,100+60*10);
    spTs.push_back(spTc);
    transform.position = vec2(ofGetWidth()/2-60*3,50);
}

void ScoreList::Draw(){
    GameObject::Draw();
    //cout<<spTs.size()<<endl;
    for(SprashText *spT : spTs){
        spT->Draw();
    }
    ofSetColor(255, 255, 255);
    ofNoFill();
    //ofDrawRectangle(transform.position.x, transform.position.y, 60*6, ofGetHeight()-50*2);
}
