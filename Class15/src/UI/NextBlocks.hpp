//
//  NextBlocks.hpp
//  Class15
//
//  Created by hw20a029 on 2023/01/03.
//

#ifndef NextBlocks_hpp
#define NextBlocks_hpp

#include <stdio.h>
#include "GameObject.hpp"
#include "Instance.h"

class NextBlocks : public GameObject{
private:
    vec2 nextColors;
    void TakeColor(int tileId);
    vec2 renderPosition;
public:
    NextBlocks();
    void Draw() override;
    void SetNextBlockColor(int leftColor, int rightColor);
};

#endif /* NextBlocks_hpp */
