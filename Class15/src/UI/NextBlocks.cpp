//
//  NextBlocks.cpp
//  Class15
//
//  Created by hw20a029 on 2023/01/03.
//

#include "NextBlocks.hpp"

NextBlocks::NextBlocks() : GameObject(){
    nextColors = vec2(0, 0);
    name = "NextBlocks";
    renderPosition = vec2(720, 120);
}

void NextBlocks::Draw(){
    GameObject::Draw();
    //実体描画
    ofSetColor(255, 255, 255, 50);
    ofFill();
    ofDrawRectangle(renderPosition.x, renderPosition.y, defaultTileSize*3, defaultTileSize+20);
    
    TakeColor(nextColors.x);
    ofDrawRectangle(renderPosition.x+defaultTileSize/2, renderPosition.y+10, defaultTileSize, defaultTileSize);
    TakeColor(nextColors.y);
    ofDrawRectangle(renderPosition.x+defaultTileSize/2+60, renderPosition.y+10, defaultTileSize, defaultTileSize);
    
    //枠線描画
    ofNoFill();
    ofSetColor(255, 255, 255, 255);
    ofSetLineWidth(2);
    ofDrawRectangle(renderPosition.x, renderPosition.y, defaultTileSize*3, defaultTileSize+20);
    ofDrawRectangle(renderPosition.x+defaultTileSize/2, renderPosition.y+10, defaultTileSize, defaultTileSize);
    ofDrawRectangle(renderPosition.x+defaultTileSize/2+60, renderPosition.y+10, defaultTileSize, defaultTileSize);
    
}

void NextBlocks::SetNextBlockColor(int leftColor, int rightColor){
    nextColors = vec2(leftColor, rightColor);
}

void NextBlocks::TakeColor(int tileId){
    switch(tileId){
        case RedDropping:
            ofSetColor(255, 100, 100);
            break;
        case BlueDropping:
            ofSetColor(100, 100, 255);
            break;
        case GreenDropping:
            ofSetColor(100, 255, 100);
            break;
        default:
            break;
            
    }
}
