//
//  Field.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/23.
//

#ifndef Field_hpp
#define Field_hpp

#include <stdio.h>
#include "GameObject.hpp"
#include "Instance.h"

class Field : public GameObject{
private:
    static const int fieldWidth = defaultFieldWidth;
    static const int fieldHeight = defaultFieldHeight;
    int fieldIds[fieldWidth][fieldHeight];
    int tileSize;
    vector<vector<float>> fallingBlockConditions; //落下中ブロックの座標とId
    int fallingFrame = 0;
    int maxFallingFrame = 3;
    vector<vec3> SetFallingBlockConditions();
    bool isFalling = false;
    bool isDropping = false;
    bool isSpawnedDropping = false;
    
    void DrawSolid();
    void DrawFalling();
    void DrawDropping();
    
    int droppingFrame = 0;
    int maxDroppingFrame = 60;
    vector<vec3> droppedBlocks; //操作中ブロックの座標とId dropが始まった瞬間に生成
    
    
public:
    Field();
    void Draw() override;
    void SetTileId(int x, int y, int tileId);
    int GetTileId(int x, int y);
    void AddFallingBlockCondition(int x, int y, int tileId);
    bool GetIsFalling();
    bool ToggleIsFalling();
    bool GetIsDropping();
    bool ToggleIsDropping();
    bool GetIsSpawnedDropping();
    bool ToggleIsSpawnedDropping();
    
    void MoveHorizontal(bool isRight);
    int AddDroppingFrame(int advanceRate);
    int GetCurrentFallingFrame();
    int GetVirusCount();
    void RotateDroppedBlocks(bool rotateRight); //0番ブロックを軸にして回転
    void SetDroppingFrame(int _droppingFrameDiff);
    vec4 backgroundColor;
};

#endif /* Field_hpp */
