//
//  Instance.h
//  Class15
//
//  Created by hw20a029 on 2022/12/27.
//

#ifndef Instance_h
#define Instance_h

#include <iostream>
using namespace std;
/*ゲーム内の共通変数の格納*/

const int defaultFieldWidth = 6;
const int defaultFieldHeight = 11;

const int defaultTileSize = 60;

const int windowSizeWidth = 1024;
const int windowSizeHeight = 768;

struct PlayerScore{
    string name;
    int score;
    PlayerScore(string str, int num){
        name = str;
        score = num;
    }
};

enum TileType{
    Void = -1,
    None = 0,
    RedVirus = 11,
    BlueVirus = 12,
    GreenVirus = 13,
    
    RedBlock = 21,
    BlueBlock = 22,
    GreenBlock = 23,
    
    RedCheckedTile = 31,
    BlueCheckedTile = 32,
    GreenCheckedTile = 33,
    
    RedFalling = 51,
    BlueFalling = 52,
    GreenFalling = 53,
    
    RedDropping = 61,
    BlueDropping = 62,
    GreenDropping = 63,
    
    Checker = 256
};



#endif /* Instance_h */
