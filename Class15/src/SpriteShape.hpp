//
//  SpriteShape.hpp
//  Class15
//
//  Created by hw20a029 on 2022/12/09.
//

#ifndef SpriteShape_hpp
#define SpriteShape_hpp

#include <iostream>

enum Shapes{
    noneshape,
    circle,
    square,
    triangle
};

#endif /* SpriteShape_hpp */
